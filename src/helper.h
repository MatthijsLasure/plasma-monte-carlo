/*
 * helper.h
 * Auxilary functions for the program
 * 
 * Author: Matthijs Lasure
 * Last modified: 16/03/2019
 */

#ifndef __HELPER_INCLUDED__
#define __HELPER_INCLUDED__

#include <cmath>
#include <fstream>
#include <iostream>
#include <sstream>
#include <string>
#include <random>
#include <vector>

#include "dataStructures.h"

namespace MC {

void readConfigFile(std::string, MC::configuration&, MC::fileNames&);

void readEfield(MC::EfieldData&, MC::configuration&, MC::fileNames&);

double limitAngle(double angle);

double hardLimit(double value, double min, double max);

long hardLimit(long value, long min, long max);

double p2(double);

void getAlpha(Electron&);

// RNG stuff
double rand();

#if __cplusplus > 199711L
extern std::mt19937_64 generator;
extern std::uniform_real_distribution<double> randdistribution;  //(0.0, 1.0);
#endif

typedef double(grid)[Nr + 1];
void InitGrid(grid* g);
void InitRes(ResultData& res);

}  // namespace MC

#endif