/*
 * datastructures.h
 * Defines some structs and variables for use in the program
 * 
 * Author: Matthijs Lasure
 * Last modified: 18/03/2019
 */

#ifndef __DATASTRUCTURES_INCLUDED__
#define __DATASTRUCTURES_INCLUDED__

#define Nz 60
#define Nr 25
#define Nrc 20
#define Nze 500
#define Nre 500
#define Ncds 20

#define mass 9.1091e-28
#define massAr 6.695e-23

#define _USE_MATH_DEFINES
#include <math.h>
#include <array>
#include <string>
#include <vector>

namespace MC {

// Initial values
struct configuration {
    double R1;      // Outer radius (cm)
    double R3;      // Inner radius (cm)
    double distz1;  // Cell length (cm)

    double dc;  // Discharge length (cm)

    // Calculated precisions
    double precz;    // Z
    double preczng;  // Z (Afterglow = nagloei)
    double precr1;   // Axial (outer)
    double precr2;   // Axial (inner)
    double precze;   // For electric field
    double precre;

    // Configuration values
    long number;     // Number of electrons
    double pres;     // Pressure (Pa)
    double temp;     // Temperature (K)
    double xnAr;     // Ar density (cm^-3)
    double xnO2;     // O2 density (cm^-3)
    double perc_Ar;  // Share of Ar (0-1)
    double je0;
    double factor;
    double surftot;

    long time_n;  // Maximum number of time steps

    double Vc;    // Applied voltage at cathode (V)
    double max;   // Maximum voltage (V)
    double tele;  // Timestep (s)
    double curr;  // Total ion flux at cathode (s^-1)
    double g;     // Secundary electron emission coefficient

    double* jfract;

    // Set default values
    configuration() {
        R1 = 0.2;
        R3 = 0.195;
        distz1 = 1.0;
        dc = 0.05;
        number = 1000;
        pres = 850.0;
        temp = 300.0;
        perc_Ar = 1.0;
        time_n = 10000000;
        Vc = 800.0;
        max = Vc + 100;
        tele = 1e-11;
        curr = -7.23e16;
        g = 0.15;
        jfract = new double[Nr + 1];
    }
    ~configuration() {
        delete[] jfract;
    }
};

// All files used in the program
struct fileNames {
    std::string Efield;
    std::string Rates;
    std::string DensEner;
    std::string EEDF;

    fileNames() {
        Efield = "Efield.dat";
        Rates = "Rates.dat";
        DensEner = "DensityEnergy.dat";
        EEDF = "EEDF.dat";
    }
};

// Struct for holding the electric field and accesories
struct EfieldData {
    // positions
    double* posze;
    double* posre;
    double* posz;
    double* posr;
    double* poszfl;

    // Efield
    typedef double(gridE)[Nre + 1];
    typedef double(grid)[Nr + 1];

    gridE* Efz;
    gridE* Efr;
    grid* Efzfl;
    grid* Efrfl;

    EfieldData() {
        posze = new double[Nze + 1];
        posre = new double[Nre + 1];
        posz = new double[Nz + 1];
        posr = new double[Nr + 1];
        poszfl = new double[Nz + 1];

        Efz = new gridE[Nze + 1];
        Efr = new gridE[Nze + 1];
        Efzfl = new grid[Nz + 1];
        Efrfl = new grid[Nz + 1];
    }
    ~EfieldData() { // Free memory after use
        delete[] posze;
        delete[] posre;
        delete[] posz;
        delete[] posr;
        delete[] poszfl;

        delete[] Efz;
        delete[] Efr;
        delete[] Efzfl;
        delete[] Efrfl;
    }
};

// Struct for storing the results
struct ResultData {
    // using grid = double (*)[Nr + 1];
    typedef double(grid)[Nr + 1];

    // Energy of electron crossing that gridpoint
    grid* Egemz;
    grid* Egemr;

    // Number of crossings through every gridpoint
    grid* numbz;
    grid* numbr;

    // Number of crossings with certain energy
    long* fen;

    // Number of crossings in positive/negative z-direction through gridpoint
    grid* positz;
    grid* positr;

    // Number of crossings in positive/negative r-direction through gridpoint
    grid* negatz;
    grid* negatr;

    // Collisions
    grid* rext;
    grid* rion;
    grid* rela;

    ResultData() {
        // Number of crossings with certain energy
        fen = new long[1000]{0};
        for (long i = 0; i < 1000; i++) {
            fen[i] = 0;
        }

        // Energy of electron crossing that gridpoint
        Egemz = new grid[Nz + 1];
        Egemr = new grid[Nz + 1];

        // Number of crossings through every gridpoint
        numbz = new grid[Nz + 1];
        numbr = new grid[Nz + 1];

        // Number of crossings in positive/negative z-direction through gridpoint
        positz = new grid[Nz + 1];
        positr = new grid[Nz + 1];

        // Number of crossings in positive/negative r-direction through gridpoint
        negatz = new grid[Nz + 1];
        negatr = new grid[Nz + 1];

        // Collisions
        rext = new grid[Nz + 1];
        rion = new grid[Nz + 1];
        rela = new grid[Nz + 1];
    }
    ~ResultData() {  // Free memory when done
        delete[] Egemz;
        delete[] Egemr;
        delete[] numbz;
        delete[] numbr;
        delete[] fen;
        delete[] positz;
        delete[] positr;
        delete[] negatz;
        delete[] negatr;
        delete[] rext;
        delete[] rion;
        delete[] rela;
    }
};

// A wild electron appears!
struct Electron {
    double x, y, z;         // Pos
    double x0, y0, z0;      // Start pos
    double vx, vy, vz;      // Speed
    double vx0, vy0, vz0;   // Start spedd
    double r, r0, alpha;    // Radius & angle (azimuthal)
    double E;               // Energy
    double theta, phi;      // Velocity angles
    long iz0, ir0, iz, ir;  // Position in the arrays

    Electron() {
        x = 0, y = 0, z = 0;
        x0 = 0, y0 = 0, z0 = 0;
        vx = 0, vy = 0, vz = 0;
        vx0 = 0, vy0 = 0, vz0 = 0;
        r = 0, r0 = 0, alpha = 0;
        E = 0;
        theta = 0, phi = 0;
        iz0 = 0, ir0 = 0, iz = 0, ir = 0;
    }
};

}  // namespace MC
#endif