/*
 * Monte Carlo simulation
 * Adapted from the original Fortran 77 code made by Annemie Bogearts
 * 
 * Author: Matthijs Lasure
 * Last modified: 18/03/2019
*/

// Standard includes
#include <chrono>
#include <cmath>
#include <ctime>
#include <iostream>
#include <vector>
#include <signal.h>

// Program includes
#include "calculations.h"
#include "dataStructures.h"
#include "helper.h"
#include "reactions.h"

int main(int argc, char *argv[]);

volatile bool keepRunning = true; // Allows to stop on CTRL-C
void signalHandler(int signal);