/*
 * helper.cpp
 * Auxilary functions for the program
 * 
 * Author: Matthijs Lasure
 * Last modified: 16/03/2019
 */

#include "helper.h"

namespace MC {
/*
 * readConfigFile
 * Get the config file and parse its contents
 * The function will exit the program if the file cannot be opened
 * File syntax: variablename: value
 * The : must be inmediatly after the variable name!
 * 
 * std::string configName: name of the file
 * configuration& vars: the structure holding the data
 * fileNames& files: the structure holding the filenames
 */
void readConfigFile(std::string configName, MC::configuration &vars, MC::fileNames &fn) {
    // Open the file
    std::ifstream cFile;
    cFile.open(configName);

    // Check if it is ok, otherwise exit with error
    if (!cFile.is_open()) {
        std::cerr << "Error opening configuration file " << configName << std::endl;
        exit(EXIT_FAILURE);
    }

    std::string line;
    //bool isVarname = true;

    // Go through the file
    while (std::getline(cFile, line)) {
        if (line.length() > 0) {  // If there is something on the line
            //std::cout << line << std::endl;
            int posDelim = line.find(':');
            if (posDelim > 0) {  // If there is a separator
                std::string name = line.substr(0, posDelim);
                std::string value = line.substr(posDelim + 2, line.length());
                std::istringstream sval(value);

                //std::cout << name << " | " << value << std::endl;

                // Set the variable
                if (name.compare("number") == 0) {
                    sval >> vars.number;
                } else if (name.compare("pres") == 0) {
                    sval >> vars.pres;
                } else if (name.compare("temp") == 0) {
                    sval >> vars.temp;
                } else if (name.compare("Vc") == 0) {
                    sval >> vars.Vc;
                } else if (name.compare("tele") == 0) {
                    sval >> vars.tele;
                } else if (name.compare("curr") == 0) {
                    sval >> vars.curr;
                } else if (name.compare("g") == 0) {
                    sval >> vars.g;
                } else if (name.compare("time_n") == 0) {
                    sval >> vars.time_n;
                } else if (name.compare("perc_Ar") == 0) {
                    sval >> vars.perc_Ar;
                } else if (name.compare("Efield") == 0) {  // Files
                    sval >> fn.Efield;
                } else if (name.compare("Rates") == 0) {
                    sval >> fn.Rates;
                } else if (name.compare("DensEner") == 0) {
                    sval >> fn.DensEner;
                } else if (name.compare("EEDF") == 0) {
                    sval >> fn.EEDF;
                }
            }
        }
    }

    // Close the file
    cFile.close();

    // Set precisions
    vars.precz = vars.dc / Ncds;
    vars.preczng = (vars.distz1 - vars.dc) / (Nz - Ncds);
    vars.precr1 = vars.R3 / Nrc;
    vars.precr2 = (vars.R1 - vars.R3) / (Nr - Nrc);
    vars.precze = vars.distz1 / Nre;
    vars.precre = vars.R1 / Nre;

    // Maximum voltage
    vars.max = vars.Vc + 100;
}

/*
 * readEfield
 * Reads the electric field from a fluid model out of a file, as defined in the fileNames structure
 * As with the readConfig function, it will exit when failing to open the file.
 * After reading, the field will be interpolated to obtain a finer grid.
 * EfieldData Efield: Structure to fill
 * configuration vars: values
 * fileNames fn: filenames
 */
void readEfield(MC::EfieldData &Efield, MC::configuration &vars, MC::fileNames &fn) {
    // Set positions
    for (long ize = 0; ize <= Nze; ize++) {
        Efield.posze[ize] = vars.precze * ize;
    }
    for (long ire = 0; ire <= Nre; ire++) {
        Efield.posre[ire] = vars.precre * ire;
    }

    // Open file
    std::ifstream eFile;
    eFile.open(fn.Efield);

    if (!eFile.is_open()) {  // Check if file is ok, exit if not
        std::cerr << "Error opening Efield file " << fn.Efield << std::endl;
        exit(EXIT_FAILURE);
    }

    // Read
    for (long iz = 0; iz <= Nz; iz++) {
        for (long ir = 0; ir <= Nr; ir++) {
            eFile >> Efield.poszfl[iz] >> Efield.posr[ir] >> Efield.Efzfl[iz][ir] >> Efield.Efrfl[iz][ir];
        }
    }

    eFile.close();

    // Interpolation
    bool doBreak = false;
    for (long ize = 0; ize <= Nze; ize++) {
        for (long ire = 0; ire <= Nre; ire++) {
            for (long iz = 0; iz <= Nz; iz++) {
                for (long ir = 0; ir <= Nr; ir++) {
                    if ((Efield.posze[ize] <= Efield.poszfl[iz + 1]) && (Efield.posze[ize] >= Efield.poszfl[iz]) &&
                        (Efield.posre[ire] <= Efield.posr[ir + 1]) && (Efield.posre[ire] >= Efield.posr[ir])) {
                        double difzefl = Efield.posze[ize] - Efield.poszfl[iz];
                        double difzfl = Efield.poszfl[iz + 1] - Efield.poszfl[iz];
                        double difrefl = Efield.posre[ire] - Efield.posr[ir];
                        double difrfl = Efield.posr[ir + 1] - Efield.posr[ir];

                        double Ezlow = Efield.Efzfl[iz][ir] + difzefl * (Efield.Efzfl[iz + 1][ir] - Efield.Efzfl[iz][ir]) / difzfl;
                        double Ezhigh = Efield.Efzfl[iz][ir + 1] + difzefl * (Efield.Efzfl[iz + 1][ir + 1] - Efield.Efzfl[iz][ir + 1]) / difzfl;

                        Efield.Efz[ize][ire] = (Ezlow + difrefl * (Ezhigh - Ezlow) / difrfl) * 1.6021e-12;

                        double Erlow = Efield.Efrfl[iz][ir] + difzefl * (Efield.Efrfl[iz + 1][ir] - Efield.Efrfl[iz][ir]) / difzfl;
                        double Erhigh = Efield.Efrfl[iz][ir + 1] + difzefl * (Efield.Efrfl[iz + 1][ir + 1] - Efield.Efrfl[iz][ir + 1]) / difzfl;

                        Efield.Efr[ize][ire] = (Erlow + difrefl * (Erhigh - Erlow) / difrfl) * 1.6021e-12;

                        // When the conditions are first met, the field is done for the current ire.
                        // We thus break out of the 2 inner loops and start the next ire.
                        doBreak = true;
                        break;
                    }
                }
                if (doBreak) {
                    doBreak = false;
                    break;
                }
            }
        }
    }
}

/*
 * limitAngle
 * Limits a value to ] 0, 2 * PI [
 * If the value exceeds these limits, 2*PI will be added or subtracted until it is inside the limits.
 * Thus the angle will stay the same
 * alpha = alpha + n * 2 * PI
 * 
 * double angle: the angle to limit
 * Returns double: the limited angle
 */
double limitAngle(double angle) {
    double twopi = 2.0 * M_PI;
    return angle - std::floor(angle / twopi) * twopi;
}

/*
 * hardLimit
 * Cuts of the value between [low, high]
 * If the value is larger it will be set to the 'high' value.
 * 
 * double value: value to limit
 * double low: the minimum value
 * double high: the maximum value
 * Returns double: the limited value
 */
double hardLimit(double value, double low, double high) {
    return std::min(std::max(value, low), high);
}

/*
 * hardLimit
 * Cuts of the value between [low, high]
 * If the value is larger it will be set to the 'high' value.
 * 
 * long value: value to limit
 * long low: the minimum value
 * long high: the maximum value
 * Returns long: the limited value
 */
long hardLimit(long value, long low, long high) {
    return std::min(std::max(value, low), high);
}

/*
 * Fast square function
 */
double p2(double a) {
    return a * a;
}

void getAlpha(Electron &e) {
    if (e.r != 0.0) {
        double alpha1 = MC::hardLimit(e.y / e.r, -1.0, 1.0);
        e.alpha = asin(alpha1);
        if (e.x < 0.0) {
            e.alpha = M_PI - e.alpha;
        }
        e.alpha = MC::limitAngle(e.alpha);
    } else {
        e.alpha = 2.0 * M_PI * MC::rand();
    }
}

// RNG back-end
/*
 * rand
 * Returns a random number in [0 1], uniform distributed
 */
// Old version of C++
#if __cplusplus <= 199711L
#warning Old compiler. Consider updating :)
double rand() {
    return ((double)std::rand() / (double)RAND_MAX);
}
#else
#warning Recent compiler <3
#ifdef FIXSEED
GEN generator{};
#else
std::random_device r;
std::mt19937_64 generator{r()};
#endif
std::uniform_real_distribution<double> randdistribution(0.0, 1.0);

double rand() {
    return randdistribution(generator);
}
#endif

void InitGrid(grid *g) {
    for (long i = 0; i < Nz; i++) {
        for (long j = 0; j < Nr; j++) {
            g[i][j] = 0.0;
        }
    }
}

void InitRes(ResultData &res) {
    // Initialize all to 0
    InitGrid(res.Egemz);
    InitGrid(res.Egemr);
    InitGrid(res.numbz);
    InitGrid(res.numbr);
    InitGrid(res.positz);
    InitGrid(res.positz);
    InitGrid(res.negatz);
    InitGrid(res.negatr);
    InitGrid(res.rext);
    InitGrid(res.rion);
    InitGrid(res.rela);
}
}  // namespace MC