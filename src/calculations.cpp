/*
 * calculations.cpp
 * Functions for actual data handling in the simulation
 * 
 * Author: Matthijs Lasure
 * Last modified: 18/03/2019
 */

#include "calculations.h"
#include <signal.h>

namespace MC {
/*
 * makePrimaryElectron
 * Create and return a new electron that starts from the cathode
 * 
 * configuration& vars: structure with basic values
 * ResultData& res: result structure (will be passed to distribution)
 * Returns: Electron
 */
MC::Electron makePrimaryElectron(MC::configuration& vars, MC::ResultData& res) {
    MC::Electron e;

    // Select starting radius
    // This depends on jfract
    double rn = MC::rand();
    for (long ir = 0; ir <= Nrc; ir++) {
        if (rn <= vars.jfract[ir]) {
            e.r0 = ir * vars.precr1;
            break;
        }
    }

    // Set coordinates
    e.alpha = 2.0 * M_PI * MC::rand();
    e.x0 = e.r0 * cos(e.alpha);
    e.y0 = e.r0 * sin(e.alpha);
    e.z0 = 0;

    // Energy
    e.E = 4.0;  // eV

    // Angles for velocity
    e.theta = acos(MC::rand());
    e.phi = 2.0 * M_PI * MC::rand();

    // Velocity
    double factor = sqrt(2.0 * e.E / mass * 1.6021e-12);
    e.vx0 = sin(e.theta) * cos(e.phi) * factor;
    e.vy0 = sin(e.theta) * sin(e.phi) * factor;
    e.vz0 = cos(e.theta) * factor;

    // Indiches
    e.iz0 = 0;
    e.ir0 = (long)(e.r0 / vars.precr1);

    // Distribution
    distribution(e.iz0, e.ir0, e.theta, e.phi, e.alpha, e.E, true, res);

    return e;
}

/*
 * makeSecundaryElectron
 * Create a new electron with given parameters
 */
MC::Electron makeSecundaryElectron(double x, double y, double z, double E, double theta, double phi, double iz0, double ir0) {
    MC::Electron e;

    // Basis
    e.x0 = x;
    e.y0 = y;
    e.z0 = z;
    e.E = E;
    e.theta = theta;
    e.phi = phi;
    e.iz0 = iz0;
    e.ir0 = ir0;

    // Velocity
    double factor = sqrt(2.0 * e.E / mass * 1.6021e-12);
    e.vx0 = sin(e.theta) * cos(e.phi) * factor;
    e.vy0 = sin(e.theta) * sin(e.phi) * factor;
    e.vz0 = cos(e.theta) * factor;

    // Radius
    e.r0 = sqrt(MC::p2(e.x0) + MC::p2(e.y0));


    // Alpha
    if (e.r0 != 0.0) {
        double alpha1 = MC::hardLimit(e.y0 / e.r0, -1.0, 1.0);
        if (e.x0 >= 0) {
            e.alpha = asin(alpha1);
        } else {
            e.alpha = M_PI - asin(alpha1);
        }
        e.alpha = limitAngle(e.alpha);
    } else {
        e.alpha = 2.0 * M_PI * MC::rand();
    }

    return e;
}

/*
 * distribution
 * Records the properties of the electrons
 * Eg: number of crossings through a gridpoint
 * 
 * double iz: index of position, axial
 * double ir: index of position, radial
 * double theta: current electron theta
 * double phi: idem
 * double alpha: idem
 * double E: idem
 * bool isZ: is this call for the axial direction then this will be true.
 * MC::ResultData& res: structure with arrays for logging
 */
void distribution(long iz, long ir, double theta, double phi, double alpha,
                  double E, bool isZ, MC::ResultData& res) {
    // Checks
    if(iz > Nz || iz < 0) {
        std::cerr << "iz Out of bounds: " << iz << std::endl;
        abort();
    }
        if(ir > Nr || ir < 0) {
        std::cerr << "ir Out of bounds: " << ir << std::endl;
        abort();
    }

    if (isZ) {
        res.numbz[iz][ir]++;
        res.Egemz[iz][ir] += E;
    } else {
        res.numbr[iz][ir]++;
        res.Egemr[iz][ir] += E;
    }

    if (isZ) {
        if (theta > M_PI / 2) {
            res.positz[iz][ir]++;
        } else {
            res.negatz[iz][ir]++;
        }
    } else {
        double angle = MC::limitAngle(phi - alpha);

        if (angle > M_PI / 2 && angle <M_PI* 1.5) {
            res.negatr[iz][ir]++;
        } else {
            res.positr[iz][ir]++;
        }
    }

    if (iz == Ncds) {
        long iE = (long)E;
        iE = hardLimit(iE, 0, 900 - 1); // TODO: replace by vars.max
        res.fen[iE]++;
    }
}

/*
 * scattering
 * Calculate new angles from scattering formulas + random
 * 
 * MC::Electron& e: current electron
 */
double scattering(MC::Electron& e) {
    if (e.E <= 0.0) e.E = 1e-5;
    double a = sqrt((exp(MC::rand() * log(1.0 + e.E)) - 1.0) / e.E);
    a = hardLimit(a, -1.0, 1.0);

    double chi = 2.0 * asin(a);
    double psi = 2.0 * M_PI * MC::rand();
    double theta2 = acos(cos(e.theta) * cos(chi) - sin(e.theta) * sin(chi) * cos(psi));

    if (theta2 == 0.0 || theta2 == M_PI) {
        e.theta = theta2;
        return chi;
    }
    if (e.theta == 0.0 || e.theta == M_PI) {
        e.phi = psi;
        e.theta = theta2;
        return chi;
    }

    double xval = (cos(e.theta) * cos(e.phi) * sin(chi) * cos(psi) - sin(e.phi) * sin(chi) * sin(psi) + sin(e.theta) * cos(e.phi) * cos(chi)) / sin(theta2);
    double yval = (cos(e.theta) * sin(e.phi) * sin(chi) * cos(psi) - cos(e.phi) * sin(chi) * sin(psi) + sin(e.theta) * sin(e.phi) * cos(chi)) / sin(theta2);

    xval = hardLimit(xval, -1.0, 1.0);

    if (yval < 0.0) {
        e.phi = 2.0 * M_PI - acos(xval);
    } else {
        e.phi = acos(xval);
    }
    e.theta = theta2;

    return chi;
}

/*
 * newPosition
 * Calculate the new position for the electron
 * 
 * Returns false if the electron is absorbed.
 */
bool newPosition(Electron& e, configuration& vars, EfieldData& Efield) {
    long ize = e.z0 / vars.precze;
    long ire = e.r0 / vars.precre;

    // Determine time step
    // This depends on the electic field and the electron energy
    double Efzr = sqrt(
        MC::p2(Efield.Efz[ize][ire]) +
        MC::p2(Efield.Efr[ize][ire]));

    double t;
    if (Efzr >= 8e-11) {
        t = vars.tele * 0.3333333333333;
    } else {
        if (e.E >= 11.55) {
            t = vars.tele;
        } else if (e.E >= 1.0) {
            t = vars.tele * 3.0;
        } else {  // Low energy, so absorb
            return false;  // Stop and go to the next electron
        }
    }  // t selection

    double common = MC::p2(t) / (2.0 * mass);
    e.x = e.x0 + e.vx0 * t - common * Efield.Efr[ize][ire] * cos(e.alpha);
    e.y = e.y0 + e.vy0 * t - common * Efield.Efr[ize][ire] * sin(e.alpha);
    e.z = e.z0 + e.vz0 * t - common * Efield.Efz[ize][ire];

    e.vx = e.vx0 - Efield.Efr[ize][ire] * t / mass * cos(e.alpha);
    e.vy = e.vy0 - Efield.Efr[ize][ire] * t / mass * sin(e.alpha);
    e.vz = e.vz0 - Efield.Efz[ize][ire] * t / mass;

    e.r = sqrt(MC::p2(e.x) + MC::p2(e.y));  // New radius

    // New energy
    e.E = 0.5 * mass * (MC::p2(e.vx) + MC::p2(e.vy) + MC::p2(e.vz)) / 1.6021e-12;

    // Calculation of the position angle from the x-axis
    MC::getAlpha(e);

    // Get new indiches
    if (e.z > vars.dc) {
        e.iz = (long)((e.z - vars.dc) / vars.preczng) + Ncds;
    } else {
        e.iz = (long)(e.z / vars.precz);
    }
    if (e.r > vars.R3) {
        e.ir = Nrc + (long)((e.r - vars.R3) / vars.precr2);
    } else {
        e.ir = (long)(e.r / vars.precr1);
    }
    e.iz = MC::hardLimit(e.iz, 0, Nz);
    e.ir = MC::hardLimit(e.ir, 0, Nr);

    // New phi
    if (e.vx == 0.0) {
        if (e.vy > 0.0) {
            e.phi = M_PI / 2.0;
        } else {
            e.phi = M_PI * 1.5;
        }
    } else if (e.vx > 0.0) {
        if (e.vy < 0.0) {
            e.phi = atan(e.vy / e.vx) + 2.0 * M_PI;
        } else {
            e.phi = atan(e.vy / e.vx);
        }
    } else {
        e.phi = atan(e.vy / e.vx) + M_PI;
    }
    // New theta
    if (e.vz == 0) {
        e.theta = M_PI / 2;
    } else {
        if (e.vx == 0.0) {
            if (e.vy == 0.0) {
                e.theta = 0.0;
            } else {
                e.theta = atan(e.vy / (sin(e.phi) * e.vz));
            }
        } else {
            e.theta = atan(e.vx / (cos(e.phi) * e.vz));
        }
    }
    e.theta = MC::limitAngle(e.theta);

    return true;  // No absorption
}

/*
 * collisionWall
 * Act when a collision has occured for both axial and radial walls
 * 
 * bool axial: true if colliding with axial wall
 * std::vector<Electron>& electrons: vector for storing a secundary electron (if formed)
 * 
 * Returns false when the original electron is absorbed
 */
bool collisionWall(MC::Electron& e, bool axial, std::vector<MC::Electron>& electrons) {
    // Delta > 1: 1 electron secundary el. em. (NG: slow group, CDS: 4 eV)
    if (e.E > 250.0) {
        double theta, phi;
        if (axial) {
            theta = acos(MC::rand());
            phi = 2.0 * M_PI * MC::rand();
        } else {
            theta = M_PI * MC::rand();
            phi = e.alpha + M_PI * 0.5 *M_PI* (2.0 * MC::rand() - 1.0);
        }
        Electron eSec = MC::makeSecundaryElectron(e.x, e.y, e.z, 4.0, theta, phi, e.iz0, e.ir0);
        electrons.push_back(eSec);
    }

    // Calculate delta (secundary electron emission coëfficient)
    double prob1 = 0.0;
    if (e.E <= 250.0) {
        prob1 = e.E / 250.0;
    } else if (e.E <= 600.0) {
        prob1 = 0.0008571 * (e.E - 250.0);
    } else {
        prob1 = 0.0002143 * (2000.0 - e.E);
    }

    double rn = MC::rand();
    if (prob1 > rn) {
        if (MC::rand() < 0.1) {
            // Reflection
            if (axial) {
                e.theta = M_PI - e.theta;
            } else {
                e.phi = limitAngle(2.0 * e.alpha - e.phi + M_PI);
            }
        } else {
            // Secundary element emission (4 eV)
            e.E = 4.0;
            if (axial) {
                e.theta = acos(MC::rand());
                e.phi = 2.0 * M_PI * MC::rand();
            } else {
                e.theta = M_PI * MC::rand();
                e.phi = limitAngle(e.alpha + M_PI + 0.5 *M_PI* (2.0 * MC::rand() - 1.0));
            }
        }
    } else {
        // Absorption
        // std::cerr << prob1 << " " << rn << std::endl;
        return false;
    }

    return true;  // No absorption
}

/*
 * missedSteps
 * If the electron has crossed more than one gridpoint, record it's properties.
 * 
 * MC::Electron& e: current electron
 * MC::ResultData& res: result structure
 */
void missedSteps(MC::Electron& e, MC::ResultData& res) {
    long idiff = e.iz - e.iz0;
    long aidiff = abs(idiff);

    if (aidiff > 1) {
        long sign = idiff / aidiff;

        for (long i = 1; i < aidiff; i++) {
            long izz = e.iz0 + i * sign;
            // std::cout << izz << " "<< i << std::endl;
            MC::distribution(izz, e.ir, e.theta, e.phi, e.alpha, e.E, true, res);
        }
    }

    idiff = e.ir - e.ir0;
    aidiff = abs(idiff);

    if (aidiff > 1) {
        long sign = idiff / aidiff;

        for (long i = 1; i < aidiff; i++) {
            long irr = e.ir0 + i * sign;
            MC::distribution(e.iz, irr, e.theta, e.phi, e.alpha, e.E, false, res);
        }
    }
}

/*
 * scatterCollision
 * The electron will lose energy and scatter
 * 
 * double deltaE: energy loss
 */
void scatterCollision(MC::Electron& e, double deltaE) {
    e.E -= deltaE;
    scattering(e);
}

/*
 * secundaryCollision
 * Upon collision, a secundary electron is created.
 * 
 * double deltaE: energy loss
 */
void secundaryCollision(MC::Electron& e, double deltaE, std::vector<MC::Electron>& electrons, MC::ResultData& res) {
    // Distribute energy - loss
    double rn = MC::rand();
    double Eprim = (e.E - deltaE) * rn;
    double Esec = (e.E - deltaE) * (1.0 - rn);

    // Secundary electron
    electrons.push_back(makeSecundaryElectron(e.x, e.y, e.z, Esec, e.theta, e.phi, e.iz0, e.ir0));

    distribution(e.iz, e.ir, e.theta, e.phi, e.alpha, Esec, true, res);

    // Primary electron
    e.E = Eprim;
    scattering(e);
}

void printResults(MC::configuration& vars, MC::fileNames& fn, MC::ResultData& res) {
    double posz[Nz + 1];
    for (long iz = 0; iz < Nz + 1; iz++) {
        if (iz <= Ncds)
            posz[iz] = iz * vars.precz;
        else
            posz[iz] = vars.dc + (iz - Ncds) * vars.preczng;
    }
    double posr[Nr + 1];
    for (long ir = 0; ir < Nr + 1; ir++) {
        if (ir <= Nrc)
            posr[ir] = ir * vars.precr1;
        else
            posr[ir] = vars.R3 + (ir - Nrc) * vars.precr2;
    }

    // Initialize 1D
    double rion1D[Nz + 1]{};
    double rext1D[Nz + 1]{};
    double rela1D[Nz + 1]{};

    // Others
    double jz1D[Nz + 1]{};
    double ne1D[Nz + 1]{};
    double Egem1D[Nz + 1]{};

    // Fluxes
    double jz[Nz + 1][Nr + 1]{};
    double jr[Nz + 1][Nr + 1]{};
    double jjz[Nz + 1][Nr + 1]{};
    double jjr[Nz + 1][Nr + 1]{};
    double ne[Nz + 1][Nr + 1]{};

    double pz = 0, pr = 0;

    // Open files
    std::ofstream rFile, deFile;
    rFile.open(fn.Rates);
    deFile.open(fn.DensEner);

    // Loop over reactor
    for (long iz = 0; iz < Nz + 1; iz++) {
        // Set to 0
        rion1D[iz] = 0.0;
        rext1D[iz] = 0.0;
        rela1D[iz] = 0.0;
        jz1D[iz] = 0.0;
        ne1D[iz] = 0.0;
        Egem1D[iz] = 0.0;

        // Define precisions
        double prec;
        if (iz == 0)  // iz = 0
            prec = vars.precz * 0.5;
        else if (iz < Ncds)  // 0 < iz < Ncds
            prec = vars.precz;
        else if (iz < Nz)  // Ncds < iz < Nz
            prec = vars.preczng;
        else  // iz = Nz
            prec = vars.preczng * 0.5;

        // Radial loop
        for (long ir = 0; ir <= Nr; ir++) {
            // E averages
            if (res.numbz[iz][ir] != 0.0) {
                res.Egemz[iz][ir] = res.Egemz[iz][ir] / res.numbz[iz][ir];
                pz = (res.positz[iz][ir] - res.negatz[iz][ir]) / res.numbz[iz][ir];
            }
            if (res.numbr[iz][ir] != 0.0) {
                res.Egemr[iz][ir] = res.Egemr[iz][ir] / res.numbr[iz][ir];
                pr = (res.positr[iz][ir] - res.negatr[iz][ir]) / res.numbr[iz][ir];
            }
            if (res.numbz[iz][ir] != 0.0 || res.numbr[iz][ir] != 0.0) {
                res.Egemz[iz][ir] = (res.numbz[iz][ir] * res.Egemz[iz][ir] + res.numbr[iz][ir] * res.Egemr[iz][ir]) / (res.numbz[iz][ir] + res.numbr[iz][ir]);
            }
            double vgem = sqrt(2.0 * res.Egemz[iz][ir] * 1.6021e-12 / mass);

            // Surface
            double surf;
            if (ir == 0)
                surf = M_PI * MC::p2(0.5 * vars.precr1);
            else if (ir <= Nrc)
                surf = 2.0 * M_PI * posr[ir] * vars.precr1;
            else
                surf = 2.0 * M_PI * posr[ir] * vars.precr2;

            double surf2;
            if (ir == 0)
                surf2 = 2.0 * M_PI * prec * 0.25 * vars.precr1;
            else
                surf2 = 2.0 * M_PI * posr[ir] * prec;

            res.rion[iz][ir] *= vars.factor / (prec * surf);
            res.rext[iz][ir] *= vars.factor / (prec * surf);
            res.rela[iz][ir] *= vars.factor / (prec * surf);
            jjz[iz][ir] = res.numbz[iz][ir] * vars.factor / surf;
            jjr[iz][ir] = res.numbr[iz][ir] * vars.factor / surf2;
            jz[iz][ir] = jjz[iz][ir] * pz;
            jr[iz][ir] = jjr[iz][ir] * pr;

            if (vgem != 0.0) {
                ne[iz][ir] = (jjz[iz][ir] + jjr[iz][ir]) / vgem;
            }
            // Calculate radialy weighted averages (convert 2D > 1D)
            if (ir <= Nrc) {
                rion1D[iz] += res.rion[iz][ir] * surf2 / vars.surftot;
                rext1D[iz] += res.rext[iz][ir] * surf2 / vars.surftot;
                rela1D[iz] += res.rela[iz][ir] * surf2 / vars.surftot;
                jz1D[iz] += jz[iz][ir] * surf2 / vars.surftot;
                ne1D[iz] += ne[iz][ir] * surf2 / vars.surftot;
                Egem1D[iz] += res.Egemz[iz][ir] * surf2 / vars.surftot;
            }
        }  // End radial for loop

        // Write results
        rFile << posz[iz] << "; " << rion1D[iz] << "; " << rext1D[iz] << "; " << rela1D[iz] << std::endl;
        deFile << posz[iz] << "; " << ne1D[iz] << "; " << Egem1D[iz] << std::endl;
    }  // End axial for loop

    // Close files
    rFile.close();
    deFile.close();

    // Get total energy
    double fTotE = 0.0;
    for (long iE = 0; iE <= vars.max; iE++) {
        fTotE += res.fen[iE];
    }

    // Write EEDF
    std::ofstream eFile;
    eFile.open(fn.EEDF);
    for (long iE = 0; iE <= vars.max; iE++) {
        eFile << iE << "; " << ((double) res.fen[iE]) / fTotE << std::endl;
    }
    eFile.close();
}

}  // namespace MC