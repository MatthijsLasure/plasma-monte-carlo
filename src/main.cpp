/*
 * Monte Carlo simulation
 * Adapted from the original Fortran 77 code made by professor doctor Annemie Bogearts
 * 
 * Author: Matthijs Lasure
 * Last modified: 18/03/2019
*/

#include "main.h"

// MAIN
// ====
int main(int argc, char** argv) {
    // Welcome!
    // ********
    std::cout << "Monte Carlo simualation" << std::endl;
    std::cout << "Build date: " << __TIMESTAMP__ << std::endl;

    // Check if a configuration file is given
    if (argc < 2) {
        std::cerr << "Error: no configuration file provided!" << std::endl;
        return -1;
    }

#if __cplusplus <= 199711L
    srand(time(NULL));
#endif

    auto startTime = std::chrono::system_clock::now();
    std::time_t sTime = std::chrono::system_clock::to_time_t(startTime);
    std::cout << "Start up: " << std::ctime(&sTime);

    // Set up variables
    // ****************
    static MC::configuration vars;
    static MC::fileNames files;

    // Read in config file
    MC::readConfigFile((std::string)argv[1], vars, files);
    std::cout << files.Efield << std::endl;

    // Densities
    vars.xnAr = vars.perc_Ar * vars.pres / (vars.temp * 1.38e-17);
    vars.xnO2 = (1.0 - vars.perc_Ar) * vars.pres / (vars.temp * 1.38e-17);

    // Electron flux at cathode
    vars.je0 = -1 * vars.g * vars.curr;
    vars.factor = vars.je0 / vars.number;

    // Get the electric field
    // **********************
    struct MC::EfieldData Efield;
    MC::readEfield(Efield, vars, files);
    std::cout << "Done reading Efield" << std::endl;

    // Set up results
    MC::ResultData res;

    // Calculate the fraction of electrons as f(r)
    // For leaving the cathode
    // Assuming uniform distribution
    vars.surftot = M_PI * pow(vars.R3, 2.0);
    for (long ir = 0; ir < Nrc; ir++) {
        double surf;
        if (ir == 0) {
            surf = M_PI * pow(0.5 * vars.precr1, 2.0);
            vars.jfract[0] = surf / vars.surftot;
        } else {
            surf = 2.0 * M_PI * ir * pow(vars.precr1, 2.0);
            vars.jfract[ir] = vars.jfract[ir - 1] + surf / vars.surftot;
        }
    }
    vars.jfract[Nrc] = 1;
    for (long ir = Nrc + 1; ir < Nr; ir++) {
        vars.jfract[Nrc] = 1;
    }

    // Create primary electrons
    // ------------------------
    std::vector<MC::Electron> electrons;
    for (long i = 0; i < vars.number; i++) {
        electrons.push_back(MC::makePrimaryElectron(vars, res));
    }

    // Set up a way to stop: use CTRL-C to gracefully stop the simulation
    // (finish current electron & do results)
    // Use kill -9 to hard kill the program
    struct sigaction sigIntHandler;
    sigIntHandler.sa_handler = signalHandler;
    sigemptyset(&sigIntHandler.sa_mask);
    sigIntHandler.sa_flags = 0;
    sigaction(SIGINT, &sigIntHandler, NULL);

    // SIMULATION TIME
    // ===============
    std::cout << "Starting simulation" << std::endl;

// Trajectory file
#ifdef PRINTTRAJECTORY
    std::ofstream trajectory;
    trajectory.open("trajectory.xyz");
#endif

    // Variables
    long secundary = 0;
    unsigned long e_index = 0;

    // While there are electrons left
    while ((e_index < electrons.size()) && keepRunning) {
        // Get next electron
        MC::Electron e = electrons[e_index++];
        std::cout << e_index << " " << electrons.size() << std::endl;

        // TIME SIMULATION
        // ***************
        for (long step = 0; step < vars.time_n; step++) {
#ifdef PRINTTRAJECTORY
            if (step % PRINTINTER == 0 && e_index == 1) {
                trajectory << 7 << std::endl;                                                   // Number of particles in the traj.
                trajectory << e_index << " " << step << " " << e.z << " " << e.E << std::endl;  // Header
                trajectory << "e " << e.x * 100 << " " << e.y * 100 << " " << e.z * 100 << std::endl;
                trajectory << "H 0 0 0\n";
                trajectory << "He 0 0 100\n";
                trajectory << "H  0 20 0\n";
                trajectory << "H 0 -20 0\n";
                trajectory << "H 20 0 0\n";
                trajectory << "H -20 0 0\n";
            }
#endif

            // Obtain new position of the electron.
            // ------------------------------------
            // The function will return false if the electron is absorbed,
            // so stop if that's the case.
            if (!MC::newPosition(e, vars, Efield)) {
                break;
            }

            // Collisions with the walls
            // -------------------------

            // Axial
            long s = electrons.size();
            if (e.z <= 0.0 || e.z >= vars.distz1) {
                e.z = MC::hardLimit(e.z, 0.0, vars.distz1);
                if (!MC::collisionWall(e, true, electrons)) {
                    break;
                }
            }

            // Radial
            if (e.r > vars.R1) {
                e.r = MC::hardLimit(e.z, 0.0, vars.R1);
                e.x = e.r * cos(e.alpha);
                e.y = e.r * sin(e.alpha);

                if (!MC::collisionWall(e, false, electrons)) {
                    break;
                }
            }

            // Missed steps
            // ------------
            MC::missedSteps(e, res);

            // Collisions :)
            // -------------
            REACT::collide(e, electrons, vars, res);

            if (electrons.size() - s > 0) {
                secundary += electrons.size() - s;
            }

            // New position and velocity
            // -------------------------
            double factor = sqrt(2.0 * e.E / mass * 1.6021e-12);
            e.vx0 = sin(e.theta) * cos(e.phi) * factor;
            e.vy0 = sin(e.theta) * sin(e.phi) * factor;
            e.vz0 = cos(e.theta) * factor;

            if (e.iz != e.iz0) {
                MC::distribution(e.iz, e.ir, e.theta, e.phi, e.alpha, e.E, true, res);
            }
            if (e.ir != e.ir0) {
                MC::distribution(e.iz, e.ir, e.theta, e.phi, e.alpha, e.E, false, res);
            }

            e.x0 = e.x;
            e.y0 = e.y;
            e.z0 = e.z;
            e.r0 = e.r;
            e.iz0 = e.iz;
            e.ir0 = e.ir;

        }  // End of time simulation
    }      // End of electron simulation

    std::cout << "Secundary: " << secundary << std::endl;

#ifdef PRINTTRAJECTORY
    trajectory.close();
#endif

    // Process and write results
    MC::printResults(vars, files, res);

    // Clean up
    // MC::TearDown(Efield, res);

    // Print some time info
    auto endTime = std::chrono::system_clock::now();
    std::time_t eTime = std::chrono::system_clock::to_time_t(endTime);
    std::chrono::duration<double> elapsed_sec = endTime - startTime;
    std::cout << "Finished at: " << std::ctime(&eTime)
              << "Wall time: " << elapsed_sec.count() << " s" << std::endl;

    return 0;  // All went well
}  // The end

/*
 * signalHandler
 * Handles the CTRL-C a user may give
 * When the SIGINT signal is received, the current electron is finished.
 * Afterwards, the stats will still be generated and written.
 * When needed, kill the program with SIGKILL when in absolute need.
 */
void signalHandler(int signal) {
    std::cerr << "Received signal " << signal << ". Clean-up & exit" << std::endl;
    keepRunning = false;
}