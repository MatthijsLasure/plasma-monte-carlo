/*
 * reactions.cpp
 * All the reactions that can occur in the reactor
 * 
 * Author: Matthijs Lasure
 * Last modified: 18/03/2019
 */

#include "reactions.h"

namespace REACT {

/*
 * Collide
 * Check if the electron collides, perform action if so
 */
void collide(MC::Electron& e, std::vector<MC::Electron>& electrons, MC::configuration& vars, MC::ResultData& res) {
    using namespace REACT;

    // Traveled distance
    double ds = sqrt(MC::p2(e.x - e.x0) + MC::p2(e.y - e.y0) + MC::p2(e.z - e.z0));

    // Probability of collision
    double Stotn = (Sext(e.E) + Sion(e.E) + Sela(e.E)) * vars.xnAr;
    double prob = 1.0 - exp(-ds * Stotn);

    // Check for collision
    double rn = MC::rand(); // TODO: elegant
    // std::cerr << prob << " | " << rn << " # " << e.E << std::endl;
    if (prob < rn) {
        return;  // No luck
    }

    // Nature of collision
    double Pext = Sext(e.E) * vars.xnAr / Stotn;
    double Pion = Pext + Sion(e.E) * vars.xnAr / Stotn;
    double Pela = Pion + Sela(e.E) * vars.xnAr / Stotn; // Double check, should be 1

    rn = MC::rand();
    // double rn = MC::rand();
    
    // Collisions
    if (rn <= Pext) {  // Total excitation of the Ar ground state
        res.rext[e.iz][e.ir]++; //++;
        MC::scatterCollision(e, 11.55);
    } else if (rn <= Pion) {  // Total ionization of the Ar ground state
        res.rion[e.iz][e.ir]++;
        MC::secundaryCollision(e, 15.8, electrons, res);
    } else {  // Elastic collision (special case)
        res.rela[e.iz][e.ir]++;
        double chi = scattering(e);
        e.E = e.E * (1.0 - 2.0 * mass / massAr * (1.0 - cos(chi)));
    }
}

// =========================================================================================================

// Ar Excitation
double Sext(double E) {
    if (E > 11.55) {
        double S = 3.4e-18 * pow(E - 11.5, 1.1) * (1.0 + pow(E / 15.0, 2.8)) /
                       (1.0 + pow(E / 23.0, 5.5)) +
                   2.3e-18 * (E - 11.5) / pow(1.0 + E / 80.0, 1.9);
        return S;
    } else
        return 0.0;
}

// Ar ionisation
double Sion(double E) {
    if (E > 15.8) {
        double S = 9.7e-14 * (E - 15.8) / MC::p2(70.0 + E) + 6e-18 * MC::p2(E - 15.8) * exp(-1.0 * E / 9);
        return S;
    } else
        return 0.0;
}

// Elastic
double Sela(double E) {
    // double S = (abs(
    //                 6.0 /
    //                     pow(
    //                         1.0 + E / 0.1 + MC::p2(E / 0.6), 3.3) -
    //                 1.1 * pow(E, 1.4) /
    //                     (1 + pow(E / 15.0, 1.2)) /
    //                     sqrt(
    //                         1.0 + pow(E / 5.5, 2.5) + pow(E / 60.0, 4.1))) +
    //             0.05 / MC::p2(1.0 + 0.1 * E) + 0.01 * pow(E, 3.0) / (1.0 + pow(E / 12.0, 6.0))) *
    //            1e-16;
    // double q = (6.0 / pow(1.0 + E / 0.1 + pow(E / 0.6, 2.0), 3.3) - 1.1 * pow(E, 1.4) / (1.0 + pow(E / 15.0, 1.2)) / pow(1.0 + pow(E / 5.5, 2.5) + pow(E / 60.0, 4.1), 0.5));
    // if (q < 0) q *= -1;
    // double S = (q + 0.05 / pow(1.0 + 0.1 * E, 2.0) + 0.01 * pow(E, 3.0) / (1.0 + pow(E / 12.0, 6.0))) * 1.0e-16;

        double q = (6.0 / pow(1.0 + E / 0.1 + MC::p2(E / 0.6), 3.3) - 1.1 * pow(E, 1.4) / (1.0 + pow(E / 15.0, 1.2)) / sqrt(1.0 + pow(E / 5.5, 2.5) + pow(E / 60.0, 4.1)));
    if (q < 0) q *= -1;
    double S = (q + 0.05 / MC::p2(1.0 + 0.1 * E) + 0.01 * E * E * E / (1.0 + pow(E / 12.0, 6.0))) * 1.0e-16;

    return S;

    // double S = (abs(6/pow(1+E/0.1+MC::p2(E/0.6), 3.3)-1.1*pow(E,1.4)/(1+pow(E/15,1.2))/sqrt(1+pow(E/5.5,2.5)+pow(E/60,4.1)))+0.05/MC::p2(1+0.1*E)+0.01*pow(E,3)/(1+pow(E/12,6)))*1e-16
}
double SionO2(double E);
double SdisionO2(double E);
double SdisO2(double E);
double SdisatO2(double E);
double SionpairO2(double E);
double SexcO2(double E);

}  // namespace REACT