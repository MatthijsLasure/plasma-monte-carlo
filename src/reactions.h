/*
 * reactions.h
 * All the reactions that can occur in the reactor
 * 
 * Author: Matthijs Lasure
 * Last modified: 18/03/2019
 */

#include <cmath>

#include "calculations.h"
#include "helper.h"

#ifndef __REACTIONS_INCLUDED__
#define __REACTIONS_INCLUDED__

namespace REACT {

// Collision logic
void collide(MC::Electron& e, std::vector<MC::Electron>& electrons, MC::configuration& vars, MC::ResultData& res);

// Cross section functions
double Sext(double E);
double Sion(double E);
double Sela(double E);
double SionO2(double E);
double SdisionO2(double E);
double SdisO2(double E);
double SdisatO2(double E);
double SionpairO2(double E);
double SexcO2(double E);

}  // namespace REACT

#endif  // __REACTIONS_INCLUDED__