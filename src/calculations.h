/*
 * calculations.h
 * Functions for actual data handling in the simulation
 * 
 * Author: Matthijs Lasure
 * Last modified: 18/03/2019
 */

#ifndef __CALCULATION_INCLUDED__
#define __CALCULATION_INCLUDED__

#include <vector>

#include "dataStructures.h"
#include "helper.h"
#include "reactions.h"

namespace MC {
Electron makePrimaryElectron(MC::configuration& vars, MC::ResultData& res);

Electron makeSecundaryElectron(double x, double y, double z, double E, double theta, double phi);

void distribution(long iz, long ir, double theta, double phi, double alpha, double E, bool isZ, MC::ResultData& res);

double scattering(MC::Electron& e);

bool newPosition(MC::Electron& e, MC::configuration& vars, MC::EfieldData& Efield);

bool collisionWall(MC::Electron& e, bool axial, std::vector<MC::Electron>& electrons);

void missedSteps(MC::Electron& e, MC::ResultData& res);

void scatterCollision(MC::Electron& e, double deltaE);
void secundaryCollision(MC::Electron& e, double deltaE, std::vector<MC::Electron>& electrons, MC::ResultData& res);

void printResults(MC::configuration& vars, MC::fileNames& fn, MC::ResultData& res);

}  // namespace MC
#endif // __CALCULATION_INCLUDED__