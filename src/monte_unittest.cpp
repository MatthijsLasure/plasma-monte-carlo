/*
 * helper_unittest.cpp
 * Unit tests for the helper functions
 * This test requires the Google Test framework
 * https://github.com/google/googletest/tree/master/googletest
 * 
 * It also requires the UT_input.cfg, Efield.dat and goodEfield.dat files to be present in the test directory.
 * 
 * Author: Matthijs Lasure
 * Last modified: 16/03/2019
 */

#include <fstream>
#include <iostream>
#include <string>

#include "calculations.h"
#include "gtest/gtest.h"
#include "helper.h"
#include "reactions.h"

// Check the variables after reading in the config files
TEST(Config, Variables) {
    // Set up variables
    MC::configuration vars;
    MC::fileNames files;

    MC::readConfigFile("UT_input.cfg", vars, files);

    EXPECT_EQ(vars.number, 987654321) << "vars.number not equal!";
    EXPECT_EQ(vars.pres, 444.2) << "vars.pres not equal!";
    EXPECT_EQ(vars.temp, 999.6) << "vars.temp not equal!";
    EXPECT_EQ(vars.Vc, 420.69) << "vars.Vc not equal!";
    EXPECT_EQ(vars.tele, 1e11) << "vars.tele not equal!";
    EXPECT_EQ(vars.curr, 14.16) << "vars.curr not equal!";
    EXPECT_EQ(vars.g, 358188) << "vars.g not equal!";
    EXPECT_EQ(vars.time_n, 123456789) << "vars.time_n not equal!";
    EXPECT_EQ(vars.perc_Ar, 1e-2) << "vars.perc_Ar not equal!";
}

// Check the filenames after reading in
TEST(Config, Files) {
    // Set up variables
    MC::configuration vars;
    MC::fileNames files;

    MC::readConfigFile("UT_input.cfg", vars, files);

    ASSERT_EQ(files.Efield, "Efield.dat");  // This has to pass or Efield will complain!
    EXPECT_EQ(files.Rates, "thisisatest.abc");
    EXPECT_EQ(files.DensEner, "TESTDENS");
    EXPECT_EQ(files.EEDF, "TEEEEEST.qed");
}

// Check the program behaves correctly when not finding the file
TEST(Config, FileNotFound) {
    // Set up variables
    MC::configuration vars;
    MC::fileNames files;

    ASSERT_DEATH({ MC::readConfigFile("/THIS/FILE/DOES/NOT/EXIST", vars, files); };, "Error opening configuration file /THIS/FILE/DOES/NOT/EXIST");
}

// Efield
// Check the program behaves correctly when not finding the file
TEST(Efield, FileNotFound) {
    // Set up variables
    MC::configuration vars;
    MC::fileNames files;
    MC::EfieldData e;

    // Set the Efield filename to something stupid
    files.Efield = "/THIS/Efield/DOES/NOT/EXIST";

    ASSERT_DEATH({ MC::readEfield(e, vars, files); };, "Error opening Efield file /THIS/Efield/DOES/NOT/EXIST");
}

// Arrays
double EZin[Nze + 1][Nre + 1];
double ERin[Nze + 1][Nre + 1];

// Check the Efield against the old monte carlo code
TEST(Efield, EfieldRead) {
    // Set up variables
    MC::configuration vars;
    MC::fileNames files;

    MC::readConfigFile("UT_input.cfg", vars, files);

    //  Read in good data from old monte
    std::string compareFile = "goodEfield.dat";
    std::ifstream cFile;
    cFile.open(compareFile);

    if (!cFile.is_open()) {
        FAIL() << "Error opening comparision file!";
    }

    // Read data via Efield
    MC::EfieldData Efield;
    MC::readEfield(Efield, vars, files);

    while (!cFile.eof()) {
        long i, j;
        double Efz_in, Efr_in;
        cFile >> i >> j >> Efz_in >> Efr_in;
        //std::cerr << i << j << std::endl;
        if (i > (Nze + 1) || j > (Nre + 1)) {
            std::cout << i << " / " << Nze + 1 << " | " << j << " / " << Nre + 1 << std::endl;
        }
        EZin[i][j] = Efz_in;
        ERin[i][j] = Efr_in;
    }
    cFile.close();

    for (long ize = 0; ize < Nze + 1; ize++) {
        for (long ire = 0; ire < Nre + 1; ire++) {
            EXPECT_NEAR(Efield.Efz[ize][ire], EZin[ize][ire], 1e-10) << "Z " << ize << " " << ire;
            EXPECT_NEAR(Efield.Efr[ize][ire], ERin[ize][ire], 1e-10) << "Z " << ize << " " << ire;
        }
    }
}

// Check the angleLimit func
TEST(limiters, angleLimit) {
    EXPECT_DOUBLE_EQ(MC::limitAngle(0), 0);                 // Inside
    EXPECT_DOUBLE_EQ(MC::limitAngle(1), 1);                 // Inside
    EXPECT_DOUBLE_EQ(MC::limitAngle(-1), -1 + 2 * PI);      // Below (1x)
    EXPECT_DOUBLE_EQ(MC::limitAngle(7), 7 - 2 * PI);        // Above (1x)
    EXPECT_DOUBLE_EQ(MC::limitAngle(-8.3), -8.3 + 4 * PI);  // Below (2x)
    // Because of the large values, errors are introduced and the results are not exact (~1e-9)
    EXPECT_NEAR(MC::limitAngle(-34833.998), -34833.998 + 11090 * PI, 1e-9);  // Below (5544x)
    EXPECT_NEAR(MC::limitAngle(1234.56789), 1234.56789 - 392 * PI, 1e-9);    // Above (196x)
}

// Check the hardLimit func with doubles
TEST(limiters, hardLimitDouble) {
    EXPECT_DOUBLE_EQ(MC::hardLimit(1.0, 0.0, 2.0), 1.0);              // Inside
    EXPECT_LE(MC::hardLimit(864843.1, 0.0, 2.0), 2.0);                // Above
    EXPECT_GE(MC::hardLimit(-5648643.84442e2, 0.0, 2.0), 0.0);        // Under
    EXPECT_DOUBLE_EQ(MC::hardLimit(sqrt(2.0), 0.0, 2.0), sqrt(2.0));  // Double check
}

// Check the hardLimit func with longs
TEST(limiters, hardLimitLong) {
    EXPECT_EQ(MC::hardLimit((long)3, 0, 5), 3);        // Inside
    EXPECT_LE(MC::hardLimit((long)444, 0, 5), 5);      // Above
    EXPECT_GE(MC::hardLimit((long)-484383, 0, 5), 0);  // Under
}

// Check crosssections
TEST(rates, Ar) {
    std::ifstream oldRates("SAr.txt");

    double oE, oSext, oSion, oSela;
    for (double i = 0; i <= 1000; i++) {
        oldRates >> oE >> oSext >> oSion >> oSela;
        ASSERT_DOUBLE_EQ(oE, i);
        EXPECT_NEAR(REACT::Sext(i), oSext, 1e-15);
        EXPECT_NEAR(REACT::Sion(i), oSion, 1e-15);
        EXPECT_NEAR(REACT::Sela(i), oSela, 1e-15);
    }
}

// New position
TEST(mainSim, newPosition) {
    // Prep
    MC::configuration vars;
    MC::fileNames files;
    MC::readConfigFile("good_input.cfg", vars, files);
    // Read data via Efield
    MC::EfieldData Efield;
    MC::readEfield(Efield, vars, files);

    // Set up results
    MC::ResultData res;

    // Set up electron
    MC::Electron e = MC::makePrimaryElectron(vars, res);
    e.x0 = 0;
    e.y0 = 0;
    e.z0 = 0;
    e.vx0 = 0;
    e.vy0 = 0;
    e.vz0 = 0;
    e.alpha = 0;

    MC::newPosition(e, vars, Efield, res);

    EXPECT_NEAR(e.x, 0, 1e-5);
    EXPECT_NEAR(e.y, 0, 1e-5);
    EXPECT_NEAR(e.z, 0.000469914317, 1e-5);

    e.x0 = 1.5;
    e.y0 = -0.75;
    e.z0 = 2.0;
    e.vx0 = 0;
    e.vy0 = 18.3;
    e.vz0 = 0.5;
    e.alpha = 5.09893942;

    MC::newPosition(e, vars, Efield, res);

    EXPECT_NEAR(e.x, 1.5, 1e-5);
    EXPECT_NEAR(e.y, -0.75, 1e-5);
    EXPECT_NEAR(e.z, 1.9999994, 1e-5);
    EXPECT_NEAR(e.E, 4.04889615e-05, 1e-5);
}