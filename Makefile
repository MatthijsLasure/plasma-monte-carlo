# Makefile
# This will transform the source code into an application
# Use 'make' to compile the code
# Use 'make clean' to remove all build artifacts & output
# Use 'make monte_unittest' to create the unit tests (if applicable)

# Get all the source files
FILES := $(wildcard src/*.cpp)
FILES := $(filter-out src/%_unittest.cpp, $(FILES)) # Exclude the unit tests
OBJS := $(patsubst src/%.cpp, build/%.o, $(FILES)) # List of object files

# Set compiler
CXX := $(shell { command -v icpc || command -v g++; } 2> /dev/null)
CXX := g++

# Flags
CXXFLAGS += -O3 -g -Wall -Wextra -pthread -std=c++0x -fstack-protector-all
# -fp-trap=all -std=c++11

# Uncomment to print trajectory (heavy files!)
#CXXFLAGS += -DPRINTTRAJECTORY -DPRINTINTER=1000

# Uncomment to fix the PRNG to a known sequence (NOT RECOMMENDED, debug only!)
#CXXFLAGS += -DFIXSEED

ifeq ($(CXX), icpc)
	CXXFLAGS += -xHost -fp-stack-check -traceback -maxv2 -no-gcc-include-dir
endif

all: montecarlo

# Main object
montecarlo: $(OBJS) Makefile
	@echo "Linking... $@"
	$(CXX) $(LDFLAGS) -o build/$@ $(OBJS) $(LIBS)

# Prepare directory
build:
	mkdir build

# Individual files
build/%.o: src/%.cpp src/%.h | build Makefile
	@echo "Building source $<"
	$(CXX) $(CXXFLAGS) -c -o $@ $<

# Clean up
.PHONY: clean
clean:
	-rm -rf build

# GOOGLE TEST
# You need the Google test framework for this to work
# If you are a student, don't worry about this ;)
# ==========================================================================
TEST_FILES := $(wildcard src/*_unittest.cpp)
TEST_OBJS := $(patsubst src/%.cpp, test/%.o, $(TEST_FILES))

GTEST_DIR := /mnt/atlas/programming/googletest/googletest

GTEST_HEADERS = $(GTEST_DIR)/include/gtest/*.h \
	$(GTEST_DIR)/include/gtest/internal/*.h
GTEST_LIBS = build/libgtest.a build/libgtest_main.a
GTEST_SRCS_ = $(GTEST_DIR)/src/*.cc $(GTEST_DIR)/src/*.h $(GTEST_HEADERS)

CPPFLAGS += -isystem $(GTEST_DIR)/include

build/gtest-all.o : $(GTEST_SRCS_)
	$(CXX) $(CPPFLAGS) -I$(GTEST_DIR) $(CXXFLAGS) -c \
			-o $@ \
            $(GTEST_DIR)/src/gtest-all.cc

build/gtest_main.o : $(GTEST_SRCS_)
	$(CXX) $(CPPFLAGS) -I$(GTEST_DIR) $(CXXFLAGS) -c \
			-o $@ \
            $(GTEST_DIR)/src/gtest_main.cc

build/libgtest.a : build/gtest-all.o
	$(AR) $(ARFLAGS) $@ $^

build/libgtest_main.a : build/gtest-all.o build/gtest_main.o
	$(AR) $(ARFLAGS) $@ $^

# Compile
sample1.o : $(USER_DIR)/sample1.cc $(USER_DIR)/sample1.h $(GTEST_HEADERS)
	$(CXX) $(CPPFLAGS) $(CXXFLAGS) -c $(USER_DIR)/sample1.cc

build/monte_unittest.o : src/monte_unittest.cpp \
                     src/helper.h src/calculations.h $(GTEST_HEADERS)
	$(CXX) $(CPPFLAGS) $(CXXFLAGS) -o $@ -c src/monte_unittest.cpp

monte_unittest : build/reactions.o build/calculations.o build/helper.o build/monte_unittest.o $(GTEST_LIBS)
	$(CXX) $(CPPFLAGS) $(CXXFLAGS) -L$(GTEST_LIB_DIR) -lgtest_main -lpthread -o test/$@ $^