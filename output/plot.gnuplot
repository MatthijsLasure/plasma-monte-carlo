#!/usr/bin/env gnuplot

set terminal qt 0
set title "Rates"
set xlabel "Distance (cm)"
set ylabel "Rate (#/m2/s)"
set ytics nomirror tc lt 1
set y2tics nomirror tc lt 2
plot "Rates.dat" u 1:2 w lines tit "Ionization", "" u 1:3 w lines tit "Excitation", "" u 1:4 w lines axes x1y2 tit "Elastic"

set terminal qt 1
set title "Density Energy"
set xlabel "Distance (cm)"
set ylabel "Density (#/m^2)" tc lt 1
set y2label "Energy (eV)" tc lt 2
set ytics nomirror tc lt 1
set y2tics nomirror tc lt 2
plot "DensityEnergy.dat" u 1:2 w lines tit "Density", "" u 1:3 w lines tit "Energy" axes x1y2

unset y2label
unset y2tics
set terminal qt 2
set title "EEDF"
set xlabel "Energy (eV)"
set ylabel "Distribution"
plot "EEDF.dat" u 1:2 w lines tit "EEDF"

pause -1

