#!/bin/bash
#PBS -N Monte
#PBS -q normal
#PBS -l nodes=1:ppn=1
#$ -S /bin/bash
#$ -cwd
#$ -j y
#$ -N "Monte"

cp ../build/montecarlo ./
./montecarlo input.cfg
