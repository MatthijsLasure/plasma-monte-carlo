#!/bin/bash

# Source files
srcFiles=$( find . \( -name \*.h -o -name \*.cpp -not -name "*_unittest.cpp" \) -print )

# Input files
inpFiles="./output/Efield.dat ./output/input.cfg ./output/run_monte.sh Makefile"

# Create archive
rm -f montecarlo.tar.bz2
tar -cjf montecarlo.tar.bz2 $srcFiles $inpFiles

# Upload
curl --raw -X POST "https://${BB_AUTH_STRING}@api.bitbucket.org/2.0/repositories/MatthijsLasure/plasma-monte-carlo/downloads" --form files=@"montecarlo.tar.bz2"
